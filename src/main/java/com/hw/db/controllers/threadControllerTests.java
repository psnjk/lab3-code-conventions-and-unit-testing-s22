package com.hw.db.controllers;

import java.sql.Timestamp;
import java.util.Collections;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;

import com.hw.db.models.Thread;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class threadControllerTests {
    private Thread stubThread;

    @BeforeEach
    @DisplayName("Create Thread")
    void createThreadTest() {
        stubThread = new Thread(
                "Jam",
                new Timestamp(456787654678L),
                "forum",
                "help",
                "slug",
                "lab is hard",
                19
        );

    }

    @Test
    @DisplayName("Create thread")
    void correctlyCreatesThreadTest() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(stubThread);
            threadController controller = new threadController();
            assertEquals(
                    ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(
                            Collections.emptyList()
                    ),
                    controller.createPost("slug", Collections.emptyList()),
                    "Creating posts"
            );
            assertEquals(stubThread, ThreadDAO.getThreadBySlug("slug"));
        }
    }

    @Test
    @DisplayName("Posts test")
    void getPostsTest() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {

            threadController controller = new threadController();

            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(stubThread);

            threadMock.when(() -> ThreadDAO.getPosts(stubThread.getId(), 10, 3, "no", false))
                    .thenReturn(Collections.emptyList());
            assertEquals(
                    ResponseEntity
                            .status(HttpStatus.OK)
                            .body(Collections.emptyList()),
                    controller.Posts("slug", 10, 3, "no", false),
                    "Post slug"
            );
        }
    }

    @Test
    @DisplayName("info test")
    void getThreadInfoTest() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug"))
                    .thenReturn(stubThread);
            threadController controller = new threadController();
            assertEquals(
                    ResponseEntity
                            .status(HttpStatus.CREATED)
                            .body(Collections.emptyList()),
                    controller.createPost("slug", Collections.emptyList()),
                    "Creating posts"
            );
            assertEquals(
                    ResponseEntity
                            .status(HttpStatus.OK)
                            .body(stubThread),
                    controller.info("slug"),
                    "Get info"
            );
        }
    }

    @Test
    @DisplayName("Delete thread test")
    void deleteThreadTest() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(stubThread);
            threadController controller = new threadController();
            stubThread.setId(2);
            assertEquals(
                    ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(Collections.emptyList()),
                    controller.createPost("slug", Collections.emptyList()),
                    "Creating posts"
            );
            assertEquals(
                    ResponseEntity
                    .status(HttpStatus.OK)
                    .body(null),
                    controller.change("slug", null),
                    "Delete thread"
            );
        }
    }

    @Test
    @DisplayName("Change thread test")
    void changeThreadTest() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadController controller = new threadController();
            String anotherSlug = "newslug";
            Thread threadToBeChanged = new Thread(
                    "Jameel",
                    new Timestamp(12323435),
                    "forum",
                    "understablen't",
                    anotherSlug,
                    "lab is still hard",
                    32
            );
            threadToBeChanged.setId(2);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(anotherSlug)).thenReturn(threadToBeChanged);
            threadMock.when(() -> ThreadDAO.getThreadById(2)).thenReturn(stubThread);
            assertEquals(
                    ResponseEntity
                    .status(HttpStatus.OK)
                    .body(stubThread),
                    controller.change(anotherSlug, stubThread),
                    "Thread was changed"
            );
        }
    }


    @Test
    @DisplayName("Create vote test")
    void createVoteTest() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug"))
                    .thenReturn(stubThread);
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                User stubUser = new User(
                        "name", "name@jameel.com", "name", "no"
                );
                userMock.when(() -> UserDAO.Info("name")).thenReturn(stubUser);
                threadController controller = new threadController();
                assertEquals(
                        ResponseEntity
                        .status(HttpStatus.OK)
                        .body(stubThread),
                        controller.createVote("slug", new Vote(stubUser.getNickname(), 32)),
                        "Passing class vote null object"
                );
            }
        }
    }
}
